<?php
/**
 * Created by contao-blank-bundle.
 * Developer: Sven Rhinow (sven@sr-tag.de)
 * Date: 29.10.18
 */



/**
 * -------------------------------------------------------------------------
 * BACK END MODULES
 * -------------------------------------------------------------------------
 */
array_insert($GLOBALS['BE_MOD']['design'], 1, array
(
    'theme_content' => array
    (
        'tables'      => array('tl_blank_modul', 'tl_blank_modul_items', 'tl_content'),
        'table'       => array('TableWizard', 'importTable'),
        'list'        => array('ListWizard', 'importList')
    )
));

/**
 * Add permissions
 */
$GLOBALS['TL_PERMISSIONS'][] = 'themecontents';
$GLOBALS['TL_PERMISSIONS'][] = 'themecontentp';

/**
 * Models
 */
$GLOBALS['TL_MODELS']['tl_contao_blank_model'] = \Srhinow\ContaoBlankModel::class;


/**
 * Hooks
 */
$GLOBALS['TL_HOOKS']['replaceInsertTags'][] = array('srhinow_contaoblank.listener.insert_tags', 'onReplaceInsertTags');

/**
 * Content elements
 */
$GLOBALS['TL_CTE']['includes']['themeArticle'] = \Srhinow\ThemecontentBundle\ContentElement\ContentThemeArticle::class;
